
module Exercise10 where

divides x = \y -> (x `mod` y) == 0

intSqrt = \x -> toInteger $ floor $ sqrt $ fromIntegral x

isPrime x =
  0 == (length $ filter (divides x) [2..n])
  where n         = intSqrt x


-- sumOfPrimesBelow x = foldr (+) 0 $ filter isPrime [2..x]

testPrime n ps =
  0 == (length $ filter (divides n) ps)


testSqrtPrime n ps =
  0 == (length $ filter (divides n) ps')
  where
    ps' = filter (< s) ps
    s   = intSqrt n

findNextPrime ps =
  takeFirst (\x -> testSqrtPrime x ps) [l..]
  where l = last ps + 1

takeFirst cond xs = take 1 $ filter cond xs


primeList n ps =
  if length ps == n then
    ps
  else
    ps' ++ findNextPrime ps'
    where
      ps' = primeList (n-1) ps


nPrimes n = primeList n [2]

-- primesBelow x =

primes = filter isPrime [2..]