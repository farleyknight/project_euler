

module Exercise5 where

divides x = \y -> (x `mod` y) == 0

dividesAll x ys = all (divides x) ys

intSqrt = \x -> toInteger $ floor $ sqrt $ fromIntegral x

isPrime x =
  0 == (length $ filter (divides x) [2..n])
  where n         = intSqrt x

findFirstFactor n =
  head $ filter (divides n) [2..s]
  where
    s = intSqrt n

factorize x =
  if isPrime x then
    [x]
  else
    let y = findFirstFactor x in
    [y] ++ (factorize $ (x `div` y))

-- The fastest way:
-- Find the prime decomposition of all the numbers from 1 to 20.
-- Find the largest power for each, e.g. 16 = 2 ** 4, so we need
-- at least 2**4 in our target number.
--
-- map factorize [1..20]
-- [[1],[2],[3],[2,2],[5],[2,3],[7],[2,2,2],[3,3],[2,5],[11],[2,2,3],[13],[2,7],[3,5],[2,2,2,2],[17],[2,3,3],[19],[2,2,5]]
--
-- This explains the following equation.
-- (NOTE: I could have fully automated this with a multiset library)
--

main = (2*2*2*2) * (3*3) * 5 * 7 * 11 * 13 * 17 * 19