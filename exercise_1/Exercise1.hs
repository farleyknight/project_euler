
module Exercise1 where

divBy3 x = x `mod` 3 == 0
divBy5 x = x `mod` 5 == 0

divBy3Or5 x = (divBy3 x) || (divBy5 x)

main = sum $ filter divBy3Or5 [1..999]

