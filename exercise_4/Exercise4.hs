

module Exercise4 where


slice from to xs = take (to - from + 1) (drop from xs)



isPalindrome xs =
  if even ls then
    (sliceXS 0 (hs - 1)) == reverse (sliceXS hs ls)
  else
    (sliceXS 0 (hs - 1)) == reverse (sliceXS (hs + 1) ls)
      where ls = length xs
            hs = ls `div` 2
            sliceXS from to = slice from to xs


isPalindromeInt :: (Show a) => a -> Bool
isPalindromeInt = isPalindrome . show

threeDigitNumbers = [100..999]

productsOf xs = [ y1 * y2 | y1 <- xs, y2 <- xs ]

main = maximum $ filter isPalindromeInt (productsOf threeDigitNumbers)