
module Exercise9 where

range   = [1..500]

doubles = [ (a,b) | a <- range, b <- range ]

-- triples = [(1,1,1)]

intSqrt = \x -> toInteger $ floor $ sqrt $ fromIntegral x

isIntSqrt x = (s*s) == x
  where
    s = intSqrt x

pyth (a,b) = isIntSqrt $ (a*a) + (b*b)

pythTriples = map toPyth $ filter pyth doubles
  where
    toPyth (a,b) = (a,b,c)
      where c = intSqrt $ (a*a) + (b*b)

sumTo1000 = filter (\x -> 1000 == sum3 x) pythTriples
  where
    sum3 (a,b,c) = a + b + c

main = mult3 $ head sumTo1000
  where
    mult3 (a,b,c) = a*b*c

