
module Exercise2 where


fourMillion = 4000000

fibList 0 = [0]
fibList 1 = [0, 1]
fibList x =
  let
    prevList = (fibList $ x - 1)
    nextNum = (last prevList) + (last $ init $ prevList)
  in
   prevList ++ [nextNum]

main = sum $ filter Prelude.even $ fibList 33


