
module Exercise7 where


divides x = \y -> (x `mod` y) == 0

dividesAll x ys = all (divides x) ys

intSqrt = \x -> toInteger $ floor $ sqrt $ fromIntegral x

isPrime x =
  0 == (length $ filter (divides x) [2..n])
  where n         = intSqrt x

findFirstFactor n =
  head $ filter (divides n) [2..s]
  where
    s = intSqrt n

factorize x =
  if isPrime x then
    [x]
  else
    let y = findFirstFactor x in
    [y] ++ (factorize $ (x `div` y))


nth n xs = head $ take 1 $ drop (n-1) xs


primes = filter isPrime [2..]

