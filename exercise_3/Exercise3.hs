
module Exercise3 where

target = 600851475143

intSqrt = \x -> toInteger $ floor $ sqrt $ fromIntegral x

isPrime x =
  0 == (length $ filter (divides x) [2..n])
  where n         = intSqrt x

divides x = \y -> (x `mod` y) == 0

findFirstFactor n =
  head $ filter (divides n) [2..s]
  where
    s = intSqrt n

factorize x =
  if isPrime x then
    [x]
  else
    let y = findFirstFactor x in
    [y] ++ (factorize $ (x `div` y))


