
module Exercise6 where

sumOfSquares xs = foldl (+) 0 $ map (\x -> x*x) xs
squareOfSum xs  = x*x
  where x = foldl (+) 0 xs


main = s2 - s1
  where
    s1 = sumOfSquares [1..100]
    s2 = squareOfSum  [1..100]